import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { FormulaireComponent } from './components/formulaire/formulaire.component';

const routes: Routes = [
  {path:'', component:AcceuilComponent},
  {path:'formulaire', component:FormulaireComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
