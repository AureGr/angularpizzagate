import { Component, NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Browser } from 'protractor';

class Pizza{
  base: String;
  ingredients: [string];
  prix: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pizzamania';
  public pizza: Pizza;

  ngOnInit(): void {
    this.pizza = new Pizza();
    this.pizza.prix = null;
  }
}
