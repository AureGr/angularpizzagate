import { Component, OnInit } from '@angular/core';

class Pizza{
  base: String;
  ingredients: [string];
  hasAnchoix: boolean;
  hasJambon: boolean;
  hasMiel: boolean;
  hasMagret: boolean;
}

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {
  title = 'pizzamania';
  public pizza: Pizza;

  constructor() { }

  ngOnInit(): void {
    this.pizza = new Pizza();
  }
  public getPrix(): number {
    let prix = 0;


    if(this.pizza.base == 'tomate')
      prix += 3;
    else if(this.pizza.base == 'creme')
      prix += 4;

    if(this.pizza.hasAnchoix)
      prix+=1;
    if(this.pizza.hasJambon)
      prix+=2;
    if(this.pizza.hasMiel)
      prix+=3;
    if(this.pizza.hasMagret)
      prix+=4;

    return prix;
  }

  isIncomplete(): boolean {
    return (this.pizza.base == null || (!this.pizza.hasAnchoix && !this.pizza.hasJambon && !this.pizza.hasMiel && !this.pizza.hasMagret))
  }
}
